# Project Info

Task manager

# Developer Info

NAME: Maxim Bakanov     
E-MAIL: dragonnirvald@gmail.com

# Software 

- JDK 1.8

- MS Windows 10

# Hardware 

- CPU: Intel i7-4770 or highter

- RAM: 16 Gb DDR3-2400MHz or highter

- ROM: 500 Gb M2.NVMe SSD or highter

# Programm run

`java -jar ./task-manager-jse.jar`

### Another files

[Screenshots](https://drive.google.com/drive/folders/1AL20oYALvGPur5pOdZJTrLM7acntcIeV?usp=sharing)

`git-shh.txt` - I used SSH at start of the education with only Windows 10 native programs. At that file it writed at Russian.

